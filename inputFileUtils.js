/**
 * Extracts the input filename from the Node.js args.
 * @returns {string} the input file's filename
 */
function extractInputFilename() {
    for (let arg of process.argv) {
        // Split each argument on the input= keyword
        const split = arg.split('input=');

        // If we have a right side, thats the input filename
        if (split.length > 1) {
            return split[1];
        }
    }

    throw ('An input file must be provided\nexample: npm start input=./inputs/1.txt');
}

/**
 * Extracts all necessary info from the input file's contents.
 * @param {string} contents - the contents of a properly formatted input file 
 * @returns {object} an object containing all info for instantiating a board object
 */
function extractInfoFromContents(contents) {
    const contentArray = contents.split(/\r?\n/);

    // Extract dimensions
    const dimensionString = contentArray.shift();
    const dimensions = dimensionString.split('x');
    const rows = dimensions[1];
    const columns = dimensions[0];

    // Extract letterArray
    const letterArray = [];
    for (let i = 0; i < rows; i++) {
        const letterArrayRow = contentArray.shift();
        letterArray.push(letterArrayRow.split(' '));
    }

    // answerWords are the remaining items in array
    const answerWords = contentArray;

    return {
        rows,
        columns,
        letterArray,
        answerWords
    };
}

module.exports = {
    extractInputFilename,
    extractInfoFromContents
};