/**
 * Class representing the wordsearch board
 */
class Board {
    constructor(rows, columns, letterArray) {
        this.rows = rows;
        this.columns = columns;
        this.letterArray = letterArray;
    }

    /**
     * Finds an array of words within the board
     * @param {string[]} wordArray - an array of words to find
     * @returns {object[]} an array of results found from the given wordArray
     */
    findWords(wordArray) {
        const results = [];

        for (let word of wordArray) {
            const result = this.findWord(word);
            if (result) {
                results.push(result);
            }
        }

        return results;
    }

    /**
     * Finds a word within the board.
     * @param {string} word - the word to find
     * @returns {undefined|object} If the word is found an object is returned
     * with the starting and ending indexes of where it was found
     */
    findWord(word) {
        // Loop through each letter on our board
        for (let row = 0; row < this.rows; row++) {
            for (let column = 0; column < this.columns; column++) {
                const result = this.search2d(row, column, word);
                if (result) {
                    return result;
                }
            }
        }
    }

    /**
     * Searches through all eight possible directions to find the given word.
     * @param {number} row - The row index to start searching on
     * @param {number} column - The column index to start searching on
     * @param {string} word - the word to search for
     * @returns {undefined|object} If the word is found an object is returned
     * with the starting and ending indexes of where it was found
     */
    search2d(row, column, word) {
        // Break out if first letter at row/col doesn't match
        // first letter of the word we're looking for
        if (this.letterArray[row][column].toUpperCase() != word[0].toUpperCase()) {
            return;
        }

        const possibleDirections = Board.getPossibleDirections();

        // Loop through all possible 8 directions for both row and cols
        for (let dirIdx = 0; dirIdx < 8; dirIdx++) {
            const xDir = possibleDirections.x[dirIdx];
            const yDir = possibleDirections.y[dirIdx];
            let nextRowIdx = row + xDir;
            let nextColIdx = column + yDir;

            let letterIdx;
            for (letterIdx = 1; letterIdx < word.length; letterIdx++) {
                /// Bounds check
                if (nextRowIdx >= this.rows || nextRowIdx < 0 ||
                    nextColIdx >= this.columns || nextColIdx < 0) {
                    break;
                }

                // Check if letter at index matches letter in word
                if (this.letterArray[nextRowIdx][nextColIdx].toUpperCase() != word[letterIdx].toUpperCase()) {
                    break;
                }

                // If all passes, increment nextIdxs to compare next letters in same direction
                nextRowIdx += xDir;
                nextColIdx += yDir;
            }

            // Lastely check if we found the whole word and return result
            if (letterIdx == word.length) {
                const result = {
                    word,
                    startingPosition: [row, column],
                    endingPosition: [nextRowIdx - xDir, nextColIdx - yDir]
                };
                return result;
            }
        }
    }

    /**
     * @returns {object} All possible direction combinations (horizontal, vertical, and diagonals)
     */
    static getPossibleDirections() {
        return {
            x: [-1, -1, -1, 0, 0, 1, 1, 1],
            y: [-1, 0, 1, -1, 1, -1, 0, 1]
        };
    }

    /**
     * Prints the findWord results in the desired format.
     * @param {object} result - Object obtained from board.findWord()
     */
    static printResult(result) {
        console.log(
            result.word + ' ' +
            result.startingPosition[0] + ':' + result.startingPosition[1] + ' ' +
            result.endingPosition[0] + ':' + result.endingPosition[1]
        );
    }
}

module.exports = Board;