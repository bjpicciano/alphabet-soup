const fs = require('fs');
const { extractInfoFromContents, extractInputFilename } = require('../inputFileUtils');
const Board = require('../board');

function getResults(filename) {
    // Read file and create the wordsearch board
    const contents = fs.readFileSync(filename, 'utf-8');
    const info = extractInfoFromContents(contents);
    const board = new Board(info.rows, info.columns, info.letterArray);

    // Search the board for the words' locations
    return board.findWords(info.answerWords);
}

describe('alphabet-soup', () => {
    it('throws an error if no input file is given', () => {
        // Only pass if the throws error and expect is called once
        expect.assertions(1);

        try {
            extractInputFilename();
        } catch (e) {
            expect(e).toBeTruthy();
        }
    });

    it('finds correct words in inputs/1.txt', () => {
        const expectation = [{
            word: 'ABC',
            startingPosition: [0, 0],
            endingPosition: [0, 2]
        },
        {
            word: 'AEI',
            startingPosition: [0, 0],
            endingPosition: [2, 2]
        }];

        const filename = 'inputs/1.txt';
        const results = getResults(filename);

        expect(results).toEqual(expectation);
    });

    it('finds correct words in inputs/2.txt', () => {
        const expectation = [{
            word: 'HELLO',
            startingPosition: [0, 0],
            endingPosition: [4, 4]
        },
        {
            word: 'GOOD',
            startingPosition: [4, 0],
            endingPosition: [4, 3]
        },
        {
            word: 'BYE',
            startingPosition: [1, 3],
            endingPosition: [1, 1]
        }];

        const filename = 'inputs/2.txt';
        const results = getResults(filename);

        expect(results).toEqual(expectation);
    });
});