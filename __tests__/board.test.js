const fs = require('fs');
const { extractInfoFromContents } = require('../inputFileUtils');
const Board = require('../board');

describe('board.js', () => {
    let board;
    let info;

    beforeAll(() => {
        // Uses inputs/2.txt for tests
        const filename = 'inputs/2.txt';
        // Read file and create the wordsearch board
        const contents = fs.readFileSync(filename, 'utf-8');
        info = extractInfoFromContents(contents);
        board = new Board(info.rows, info.columns, info.letterArray);
    });

    it('finds words in all 8 directions', () => {
        const words = [
            'BLY', 'LBF', 'LZX', 'JVO',
            'AEK', 'FB', 'DOOG', 'OLLEH'
        ];

        const results = board.findWords(words);

        expect(results.length).toBe(8);
    });

    it('returns undefined if no word is found', () => {
        const result = board.findWord('NOTIN');

        expect(result).toBeUndefined();
    });

    it('prints the correctly format result', () => {
        const expectation = ['HELLO 0:0 4:4', 'GOOD 4:0 4:3', 'BYE 1:3 1:1'];

        // Save original functionality
        const CONSOLE_LOG = console['log'];

        // Mock console.log to keep track of outputs
        let output = [];
        console['log'] = jest.fn((log) => output.push(log));

        const results = board.findWords(info.answerWords);
        for (let result of results) {
            Board.printResult(result);
        }

        // Restore original functionality
        console['log'] = CONSOLE_LOG;

        expect(output).toEqual(expectation);
    });
});