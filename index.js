const fs = require('fs');
const { extractInputFilename, extractInfoFromContents } = require('./inputFileUtils');
const Board = require('./board');

const filename = extractInputFilename();

// Read file and create the wordsearch board
const contents = fs.readFileSync(filename, 'utf-8');
const info = extractInfoFromContents(contents);
const board = new Board(info.rows, info.columns, info.letterArray);

// Search the board for the words' locations
const results = board.findWords(info.answerWords);
for (let result of results) {
    Board.printResult(result);
}